
(function($) {
    const shuffleArray = (arr) => {
      let currentIndex = arr.length;
      let temporaryValue;
      let randomIndex;
      // While there remain elements to shuffle...
      while (currentIndex !== 0) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = arr[currentIndex];
        arr[currentIndex] = arr[randomIndex];
        arr[randomIndex] = temporaryValue;
      }
      return arr;
    };
  
    //Each object contains the question, an array of the possible answers, and the correct answer.
    let questions = [
      {
        text: "Qui est Zlatan Ibrahimovic ?",
        options: ['Un joueur de football suédois', 'Un joueur de football marseillais', 'Un joueur de football croate', 'Un joueur de football serbe'],
        answerIndexInOptions: 0
      },
      {
        text: "Quelle est la périodicité des jeux Olympiques d’été ?",
        options: ['Tous les 50 ans', 'Tous les deux ans', 'Toutes les semaines', 'Tous les quatres ans'],
        answerIndexInOptions: 1
      },
      {
        text: "Lors du Tour de France cycliste, qui est récompensé d’un maillot blanc à pois rouges ?",
        options: ['Le meilleur jeune coureur', 'Le meilleur sprinter', 'Le meilleur grimpeur', 'Le meilleur des meilleurs'],
        answerIndexInOptions: 2
      },
      {
        text: "Où se sont déroulés les jeux Olympiques d’été en 2016 ?",
        options: ['Rio de Janeiro', 'Belsunce', 'Chicago', 'Doha'],
        answerIndexInOptions: 0
      },
      {
        text: "Quel nom porte un terrain de tennis ?",
        options: ['La surface', 'Le stade vélodrome', 'La terre battue', 'Le court'],
        answerIndexInOptions: 3
      },
      {
        text: "Dans quel sport emploie-t-on les termes suivants : split, spare, strike ?",
        options: ['Au ketchup', 'Au bowling', 'Au billard', 'Au ping-pong'],
        answerIndexInOptions: 1
      },
      {
        text: "Au judo, quel est le grade le plus élevé parmi ces ceintures ?",
        options: ['Rouge', 'Noire', 'Cuir', 'Verte'],
        answerIndexInOptions: 0
      },
      {
        text: "Dans quelle discipline sportive s’est illustré Sebastien Loeb ?",
        options: ['Motocross', 'Golf', 'Rallye automobile sur route', 'Kung Fu des Pandas !'],
        answerIndexInOptions: 2
      },
      {
        text: "Laquelle de ces courses à la voile est une course en solitaire ?",
        options: ['Le Volvo Océan Race', 'La course de l Huveaune', 'Le Vendée Globe', 'Le trophée Jules Verne'],
        answerIndexInOptions: 2
      },
      {
        text: "En rugby, quel pays ne participe pas au tournoi des Six-Nations ?",
        options: ['La France', 'L\'Espagne', 'L\'Angleterre', 'L\'Italie'],
        answerIndexInOptions: 1
      },
    ];
    questions.shuffleOptions = () => {
      questions.forEach( el => {
        const correctAnswer = el.options[el.answerIndexInOptions];
        el.options = shuffleArray(el.options);
        el.answerIndexInOptions = el.options.indexOf(correctAnswer);
      } );
    };
  
    const quiz = (function() {
      const getFinalResultsString = (score, questionsCount) => {
        return `Your final score is ${score} out of ${questionsCount}.`;
      };
  
      const getIncorrectAnswerString = (correctAnswer) => {
        return `You should have chosen ${correctAnswer}.`;
      };
  
      const showPopUp = (popUp, text) => {
        $('main, header').addClass('body-transparent');
        popUp
          .fadeIn()
          .find('.pop-up-text')
          .text( text );
      };
  
      const hidePopUp = (popUp) => {
        $('main, header').removeClass('body-transparent');
        popUp.fadeOut('fast');
        popUp.find('pop-up-text').empty();
      };
  
      const setQuestionCount = (num, questionsCount) => {
        $('.current-question-div').text(
          `Questions: ${num}/${questionsCount}`
        );
      };
  
      const setScoreCount = (score, questionsCount) => {
        $('.current-score-div').text(
          `Score: ${score}/${questionsCount}`
        );
      };
  
      const setOption = (answerElem, optionIndex, optionText) => {
        $(answerElem)
          .find('input[type="radio"]')
          .prop('checked', optionIndex === 0)
          .attr( {
            'id': optionText,
            'value': optionText,
            'data-index': optionIndex,
          } );
        $(answerElem)
          .find('label')
          .attr( 'for', optionText )
          .text( optionText );
      };
  
      const setQuestion = (question) => {
        $('.quiz-questions').text( question.text );
        $('.answer-to-question').each(
          (index, elem) => setOption(elem, index, question.options[index])
         );
      };
  
      const showResults = (score, questionsCount) => {
        $('.final-results').removeClass('hidden');
        $('.final-score-paragraph').text(
          getFinalResultsString(score, questionsCount)
         );
      };
  
      const questionsCount = questions.length;
      let currentQuestionIndex = 0;
      let score = 0;
  
      return {
        start: () => {
          $('.quiz-div').removeClass('hidden');
          $('.start-page-div').addClass('hidden');
          questions = shuffleArray(questions);
          questions.shuffleOptions();
          setScoreCount( score, questionsCount );
          setQuestion( questions[currentQuestionIndex] );
          setQuestionCount( currentQuestionIndex, questionsCount );
        },
        reset: () => {
          currentQuestionIndex = 0;
          score = 0;
          $('.start-page-div').removeClass('hidden');
          $('.final-results').addClass('hidden');
        },
        handleAnswer: () => {
          const isAnswerCorrect = questions[currentQuestionIndex].answerIndexInOptions === Number(
              $('.answer-to-question input:checked').attr('data-index')
            );
          const correctAnswer =
            questions[currentQuestionIndex].options[questions[currentQuestionIndex].answerIndexInOptions];
          if ( isAnswerCorrect ) {
            showPopUp( $('.pop-outer-correct'), '' );
            score++
          }
          else {
            showPopUp(
              $('.pop-outer-incorrect'),
              getIncorrectAnswerString( correctAnswer )
            );
          }
          setScoreCount( score, questionsCount );
        },
        proceed: () => {
          currentQuestionIndex++;
          // if quiz has not ended yet
          if ( currentQuestionIndex < questionsCount ) {
            setQuestion( questions[currentQuestionIndex] );
            setQuestionCount( currentQuestionIndex, questionsCount );
          } else {
            $('.quiz-div').addClass('hidden');
            showResults( score, questionsCount );
          }
        },
        hidePopUp: hidePopUp,
      };
    })();
    
    const submitQuestion = () => {
      quiz.handleAnswer();
      quiz.proceed();
    };
    
    const closePopUp = ev => 
      quiz.hidePopUp( $(ev.target).parents('.pop-outer') );
  
  
    const events = [
      {
        'type': 'click',
        'selector': '.start-button', 
        'handler': quiz.start,
      },
      {
        'type': 'click',
        'selector': '.start-over', 
        'handler': quiz.reset,
      },
      {
        'type': 'click',
        'selector': '.submit-question', 
        'handler': submitQuestion,
      },
      {
        'type': 'click',
        'selector': '.pop-outer .close', 
        'handler': closePopUp,
      },
    ];
  
    events.forEach( (ev) => {
      $(document).on( ev.type, ev.selector, ev.handler);
    } );
    
  })($);